module.exports = function(grunt) {
  'use strict';

  var modules = [
    './app/main.js.coffee',
    './app/services/*.js.coffee',
    './app/directives/*.js.coffee',
    './app/filters/*.js.coffee',
    './app/controllers/*.js.coffee'
  ];

  var vendor = [
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/angular/angular.min.js',
    './node_modules/slick-carousel/slick/slick.min.js',
    './node_modules/angular-slick-carousel/dist/angular-slick.min.js',
  ];

  grunt.initConfig({
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          './dist/main.css': './assets/css/main.scss'
        }
      }
    },
    coffee: {
      options: {
        bare: true
      },
      compile: {
        files: {
          './dist/application.js': modules
        }
      }
    },
    uglify: {
      options: {
        mangle: false
      },
      production: {
        files: {
          './dist/application.min.js': './dist/application.js',
          './dist/frameworks.min.js': vendor
        }
      }
    },
    watch: {
      css: {
        files: [
          './assets/css/**/*',
        ],
        tasks: ['sass']
      },
      script: {
        files: [
          './app/**/*',
          './app/controllers/*'
        ],
        tasks: ['coffee:compile','uglify:production']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['sass','coffee:compile','uglify','watch']);
};
