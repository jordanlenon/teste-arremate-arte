@app.directive 'modal', [() ->
  return {
    transclude: true,
    scope: {},
    template: '' +
    '<div class="modal" id="{{target}}">' +
      '<div class="modal-overlay" ng-click="close()"></div>' +
      '<span class="modal-close fa fa-times" ng-click="close()"></span>' +
      '<div class="modal-content" ng-transclude></div>' +
    '</div>'
    link: ($scope, element, attributes) ->
      $scope.target = attributes.target

      $scope.close = ->
        $('.modal').fadeOut(50)
        return
      # $scope.close()
    # link()
  }
] #modal
