@app.service 'HomeService', ['$http', ($http) ->
  self = this

  self.loadAllItems = (success, error) ->
    $http.get("//jsonplaceholder.typicode.com/photos?_limit=20").then (response) ->
      success(response.data) if success
    , (response) ->
      error(response) if error
    # http()
  # loadAllItems

  return
] # CampaignsService
