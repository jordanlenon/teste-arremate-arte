@app.controller 'ApplicationController', ['$scope', ($scope) ->

  $scope.navItems = [
    { text: 'Avaliar peças', path: '#review' }
    { text: 'Sobre', path: '#about' }
    { text: 'Como funciona', path: '#works' }
    { text: 'Contato', path: '#contact' }
    { text: 'Leilões', path: '#' }
  ] # navItems

  $scope.toggleMenu = ->
    $scope.menuOpen = !$scope.menuOpen
    return
  # $scope.toggleMenu()

  $scope.showModal = (target) ->
    $(target).fadeIn(100)
    return
  # $scope.showModal

  $scope.hideModal = (target) ->
    $(target).fadeOut(100)
    return
  # $scope.showModal

  return
] # ApplicationController


