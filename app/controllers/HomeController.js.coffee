@app.controller 'HomeController', ['$scope', 'HomeService', ($scope, HomeService) ->

  $scope.carouselSettings = {
    infinite: true
    slidesToShow: 3
    slidesToScroll: 1
    autoplay: true
    autoplaySpeed: 8000
    centerMode: true
    event: 
      afterChange: (event, slick, currentSlide, nextSlide) ->
        event.preventDefault()
        changeCurrentSlideBackground(currentSlide)
      # afterChange
      init: (event, slick) ->
        event.preventDefault()
        changeCurrentSlideBackground(slick.currentSlide)
      # init
    responsive: [{
        breakpoint: 999
        settings: {
          slidesToShow: 1
          centerMode: false
        }
      }
    ]
  }

  $scope.carouselItems = [
    { 
      title: 'Arte 1'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28837/2d72d44d0724b10fc5fe1281afceeb53d1aff6df_csl.JPG'
      path: '#124'
      value: 1200
      reference: 124
      isSold: false
    },
    { 
      title: 'Arte 2'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28836/3b34d9c43ad43097319ff21ba7bb546616c77b9c_csl.JPG'
      path: '#124'
      value: 850
      reference: 125
      isSold: false
    },
    { 
      title: 'Arte 3'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28837/2d72d44d0724b10fc5fe1281afceeb53d1aff6df_csl.JPG'
      path: '#124'
      value: 499
      reference: 126
      isSold: true
    },
    { 
      title: 'Arte 4'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28836/3b34d9c43ad43097319ff21ba7bb546616c77b9c_csl.JPG'
      path: '#124'
      value: 1400
      reference: 127
      isSold: false
    },
    { 
      title: 'Arte 5'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28837/2d72d44d0724b10fc5fe1281afceeb53d1aff6df_csl.JPG'
      path: '#124'
      value: 1250
      reference: 128
      isSold: false
    }
  ]

  $scope.cardItems = [
    { 
      title: 'Consectetur adipiscing 1'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28837/2d72d44d0724b10fc5fe1281afceeb53d1aff6df_csl.JPG'
      path: '#124'
    },
    { 
      title: 'Lorem ipsum 2'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51572/e0fbdcfddaeb7f8cfc66fc392c684dd67e6531bb_cl.JPG'
      path: '#124'
    },
    { 
      title: 'Sit amet 3'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51978/036b526e8dcf64d97fb9895f2b4df948271d85fd_cl.JPG'
      path: '#124'
    },
    { 
      title: 'Dolor sit amet 4'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51632/90ae07c1c6df9ca7712d85c7e9f5131d90da2ac6_cl.JPG'
      path: '#124'
    },
    { 
      title: 'Consectetur adipiscing 5'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51279/6ca404d2efce625eac89e7d94a6ad360f59d40a4_cl.JPG'
      path: '#124'
    },
    { 
      title: 'Ut mollis 6'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51132/55cbc83e3026583ec403f97a0bc460c8995a1b5a_cl.JPG'
      path: '#124'
    },
    { 
      title: 'Justo suscipit 7'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51477/8eb923059453ed84598eddcaeedd8183fcb9506b_cl.JPG'
      path: '#124'
    },
    { 
      title: 'Suscipit 8'
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.'
      image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51087/008444aa0b2531c8d7c1119a5abbc9f024052bab_cl.JPG'
      path: '#124'
    }
  ]

  $scope.moreItemsCarouselSettings = {
    infinite: true
    slidesToShow: 5
    slidesToScroll: 5
    responsive: [{
        breakpoint: 999
        settings: {
          slidesToShow: 3
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 640
        settings: {
          slidesToShow: 1
          slidesToScroll: 1
        }
      }
    ]
  }

  HomeService.loadAllItems (response) ->
    $scope.itemsLoaded = true
    $scope.moreItems = response
  , ->
    console.log('Ocorreu um erro ao processar os itens.')
  # HomeService.loadAllItems

  $scope.goToSlide = (index) ->
    if $('.home-carousel .slick-slider').slick('slickCurrentSlide') != index
      $('.home-carousel .slick-slider').slick 'slickGoTo', index
    return
  # $scope.goToSlide()

  changeCurrentSlideBackground = (index) ->
    backgroundUrl = $(".home-carousel .slide-item[data-slick-index=#{index}]").find('.img-responsive').attr('src')
    $('.home-carousel .carousel-mask').fadeOut(0)
    $('.home-carousel .carousel-mask').fadeIn(300)
    $('.home-carousel .carousel-mask').css('background-image', "url(#{backgroundUrl})")
    return
  # changeCurrentSlideBackground()

  $scope.showImage = (src) ->
    $scope.currentImageModal = null
    $scope.currentImageModal = src
    $scope.showModal('#show-image')
    return false
  # $scope.showImage()

  $(window).on 'scroll', (event) ->
    target = event.currentTarget
    if target.scrollY >= 50
      $('#header').addClass 'scrolling'
    else
      $('#header').removeClass 'scrolling'
    # end
  # onScroll

  return
] # HomeController
