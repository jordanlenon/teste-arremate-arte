this.app = angular.module('Galeria', ['slickCarousel']);

this.app.service('HomeService', [
  '$http', function($http) {
    var self;
    self = this;
    self.loadAllItems = function(success, error) {
      return $http.get("//jsonplaceholder.typicode.com/photos?_limit=20").then(function(response) {
        if (success) {
          return success(response.data);
        }
      }, function(response) {
        if (error) {
          return error(response);
        }
      });
    };
  }
]);

this.app.directive('modal', [
  function() {
    return {
      transclude: true,
      scope: {},
      template: '' + '<div class="modal" id="{{target}}">' + '<div class="modal-overlay" ng-click="close()"></div>' + '<span class="modal-close fa fa-times" ng-click="close()"></span>' + '<div class="modal-content" ng-transclude></div>' + '</div>',
      link: function($scope, element, attributes) {
        $scope.target = attributes.target;
        return $scope.close = function() {
          $('.modal').fadeOut(50);
        };
      }
    };
  }
]);

this.app.filter('brCurrency', function() {
  return function(number) {
    var c, d, decimal_sep, decimals, i, j, n, sign, t, thousands_sep;
    decimals = 2;
    decimal_sep = ',';
    thousands_sep = '.';
    n = number;
    c = (isNaN(decimals) ? 2 : Math.abs(decimals));
    d = decimal_sep || ".";
    t = (typeof thousands_sep === "undefined" ? "," : thousands_sep);
    sign = (n < 0 ? "-" : "");
    i = parseInt(n = Math.abs(n).toFixed(c)) + "";
    j = ((j = i.length) > 3 ? j % 3 : 0);
    if (!isNaN(number)) {
      return 'R$ ' + sign + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    } else {
      return 'R$ 0,00';
    }
  };
});

this.app.controller('ApplicationController', [
  '$scope', function($scope) {
    $scope.navItems = [
      {
        text: 'Avaliar peças',
        path: '#review'
      }, {
        text: 'Sobre',
        path: '#about'
      }, {
        text: 'Como funciona',
        path: '#works'
      }, {
        text: 'Contato',
        path: '#contact'
      }, {
        text: 'Leilões',
        path: '#'
      }
    ];
    $scope.toggleMenu = function() {
      $scope.menuOpen = !$scope.menuOpen;
    };
    $scope.showModal = function(target) {
      $(target).fadeIn(100);
    };
    $scope.hideModal = function(target) {
      $(target).fadeOut(100);
    };
  }
]);

this.app.controller('HomeController', [
  '$scope', 'HomeService', function($scope, HomeService) {
    var changeCurrentSlideBackground;
    $scope.carouselSettings = {
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 8000,
      centerMode: true,
      event: {
        afterChange: function(event, slick, currentSlide, nextSlide) {
          event.preventDefault();
          return changeCurrentSlideBackground(currentSlide);
        },
        init: function(event, slick) {
          event.preventDefault();
          return changeCurrentSlideBackground(slick.currentSlide);
        }
      },
      responsive: [
        {
          breakpoint: 999,
          settings: {
            slidesToShow: 1,
            centerMode: false
          }
        }
      ]
    };
    $scope.carouselItems = [
      {
        title: 'Arte 1',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28837/2d72d44d0724b10fc5fe1281afceeb53d1aff6df_csl.JPG',
        path: '#124',
        value: 1200,
        reference: 124,
        isSold: false
      }, {
        title: 'Arte 2',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28836/3b34d9c43ad43097319ff21ba7bb546616c77b9c_csl.JPG',
        path: '#124',
        value: 850,
        reference: 125,
        isSold: false
      }, {
        title: 'Arte 3',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28837/2d72d44d0724b10fc5fe1281afceeb53d1aff6df_csl.JPG',
        path: '#124',
        value: 499,
        reference: 126,
        isSold: true
      }, {
        title: 'Arte 4',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28836/3b34d9c43ad43097319ff21ba7bb546616c77b9c_csl.JPG',
        path: '#124',
        value: 1400,
        reference: 127,
        isSold: false
      }, {
        title: 'Arte 5',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28837/2d72d44d0724b10fc5fe1281afceeb53d1aff6df_csl.JPG',
        path: '#124',
        value: 1250,
        reference: 128,
        isSold: false
      }
    ];
    $scope.cardItems = [
      {
        title: 'Consectetur adipiscing 1',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_spotlight_photos/28837/2d72d44d0724b10fc5fe1281afceeb53d1aff6df_csl.JPG',
        path: '#124'
      }, {
        title: 'Lorem ipsum 2',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51572/e0fbdcfddaeb7f8cfc66fc392c684dd67e6531bb_cl.JPG',
        path: '#124'
      }, {
        title: 'Sit amet 3',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51978/036b526e8dcf64d97fb9895f2b4df948271d85fd_cl.JPG',
        path: '#124'
      }, {
        title: 'Dolor sit amet 4',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51632/90ae07c1c6df9ca7712d85c7e9f5131d90da2ac6_cl.JPG',
        path: '#124'
      }, {
        title: 'Consectetur adipiscing 5',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51279/6ca404d2efce625eac89e7d94a6ad360f59d40a4_cl.JPG',
        path: '#124'
      }, {
        title: 'Ut mollis 6',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51132/55cbc83e3026583ec403f97a0bc460c8995a1b5a_cl.JPG',
        path: '#124'
      }, {
        title: 'Justo suscipit 7',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51477/8eb923059453ed84598eddcaeedd8183fcb9506b_cl.JPG',
        path: '#124'
      }, {
        title: 'Suscipit 8',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris gravida consectetur neque, ut mollis justo suscipit ac.',
        image: '//s3-sa-east-1.amazonaws.com/arrematearte-farm/cristinagoston/lot_photos/51087/008444aa0b2531c8d7c1119a5abbc9f024052bab_cl.JPG',
        path: '#124'
      }
    ];
    $scope.moreItemsCarouselSettings = {
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      responsive: [
        {
          breakpoint: 999,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        }, {
          breakpoint: 640,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    HomeService.loadAllItems(function(response) {
      $scope.itemsLoaded = true;
      return $scope.moreItems = response;
    }, function() {
      return console.log('Ocorreu um erro ao processar os itens.');
    });
    $scope.goToSlide = function(index) {
      if ($('.home-carousel .slick-slider').slick('slickCurrentSlide') !== index) {
        $('.home-carousel .slick-slider').slick('slickGoTo', index);
      }
    };
    changeCurrentSlideBackground = function(index) {
      var backgroundUrl;
      backgroundUrl = $(".home-carousel .slide-item[data-slick-index=" + index + "]").find('.img-responsive').attr('src');
      $('.home-carousel .carousel-mask').fadeOut(0);
      $('.home-carousel .carousel-mask').fadeIn(300);
      $('.home-carousel .carousel-mask').css('background-image', "url(" + backgroundUrl + ")");
    };
    $scope.showImage = function(src) {
      $scope.currentImageModal = null;
      $scope.currentImageModal = src;
      $scope.showModal('#show-image');
      return false;
    };
    $(window).on('scroll', function(event) {
      var target;
      target = event.currentTarget;
      if (target.scrollY >= 50) {
        return $('#header').addClass('scrolling');
      } else {
        return $('#header').removeClass('scrolling');
      }
    });
  }
]);
